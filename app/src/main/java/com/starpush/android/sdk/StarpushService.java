package com.starpush.android.sdk;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public abstract class StarpushService extends Service {

    private static final String TAG = StarpushService.class.getSimpleName();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO check source of the intent and show popup on first time to user if it is not a system app
        // TODO check signature of known apps
        Log.d(TAG, "onStartCommand: intent received. Intent: " + intent.toString());

        String action = intent.getAction();

        if (action != null && action.equals(Constants.ACTION_PUSH_RECEIVE)) {
            String extraPushToken = intent.getStringExtra(Constants.EXTRA_PUSH_TOKEN);
            String extraMessagePayload = intent.getStringExtra(Constants.EXTRA_MESSAGE_PAYLOAD);
            String extraMessageToken = intent.getStringExtra(Constants.EXTRA_MESSAGE_TOKEN);
            if (extraPushToken != null) {
                // store the current push token
                Log.d(TAG, "Received push token " + extraPushToken);
                PushSettingHelper.setPushToken(getApplicationContext(), extraPushToken);
                onNewToken(extraPushToken);
            } else if (extraMessagePayload != null && extraMessageToken != null) {
                // todo enhance message passing
                Log.d(TAG, "Received new message with token " + extraMessageToken + " and payload "  + extraMessagePayload);
                onMessageReceived(new RemoteMessage(extraMessagePayload, null));
                sendMessageAck(extraMessageToken);
            }
        }
        return START_NOT_STICKY;
    }

    private void sendMessageAck(String messageToken) {
        Log.d(TAG, "sending message ack");
        Intent messageAckIntent = new Intent();
        messageAckIntent.setAction(Constants.ACTION_PUSH_MESSAGE_ACK);
        messageAckIntent.setPackage("com.starpush.android.pushservice");
        messageAckIntent.putExtra(Constants.EXTRA_MESSAGE_TOKEN, messageToken);
        getApplicationContext().sendBroadcast(messageAckIntent);
        Log.d(TAG, "sent message ack");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public abstract void onMessageReceived(RemoteMessage message);

    public abstract void onNewToken(String token);
}
