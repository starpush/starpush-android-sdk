package com.starpush.android.sdk;

public final class Constants {
    public static final String ACTION_PUSH_RECEIVE = "com.starpush.android.intent.action.RECEIVE";
    public static final String ACTION_PUSH_REGISTER = "com.starpush.android.intent.action.REGISTER";
    public static final String ACTION_PUSH_UNREGISTER = "com.starpush.android.intent.action.UNREGISTER";
    public static final String ACTION_PUSH_MESSAGE_ACK = "com.starpush.android.intent.action.MESSAGE_ACK";

    public static final String EXTRA_PUSH_TOKEN = "push_token";
    public static final String EXTRA_MESSAGE_PAYLOAD = "message_payload";
    public static final String EXTRA_MESSAGE_TOKEN = "message_token";
    public static final String EXTRA_PACKAGE_NAME = "package_name";

}
