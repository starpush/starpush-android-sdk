package com.starpush.android.sdk;

public class RemoteMessage {
    private final String payload;
    private final String subscriptionToken;

    public RemoteMessage(String payload, String subscriptionToken) {
        this.payload = payload;
        this.subscriptionToken = subscriptionToken;
    }

    public String getPayload() {
        return payload;
    }

    public String getSubscriptionToken() {
        return subscriptionToken;
    }
}
