package com.starpush.android.sdk;

import android.content.Context;

public class PushSettingHelper {
    private static final String PREFERENCE_PUSH_TOKEN = "push_token";

    private static final String PREFERENCES = "com.starpush.android.sdk.prefs";

    static String getPushToken(Context context) {
        return getStringSetting(context, PREFERENCE_PUSH_TOKEN, null);
    }

    static void setPushToken(Context context, String pushToken) {
        setStringSetting(context, PREFERENCE_PUSH_TOKEN, pushToken);
    }

    static String getStringSetting(Context context, String key, String defaultValue) {
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).getString(key, defaultValue);
    }

    static void setStringSetting(Context context, String key, String value) {
        context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit().putString(key, value).apply();
    }
}
