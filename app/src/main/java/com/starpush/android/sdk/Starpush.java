package com.starpush.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Starpush {

    private static final String TAG = Starpush.class.getSimpleName();

    /**
     * If this function returns null there will be a call to onNewToken in the StarpushService in the future
     *
     * @return null if no token yet available, else the current token
     */
    public static String getPushToken(Context context) {
        Log.d(TAG, "Retrieving push token");

        String token = PushSettingHelper.getPushToken(context);
        // TODO find some way to validate the token so we know it is not from backup?
        if (token != null) {
            Log.d(TAG, "Found token in storage token");
            return token;
        }

        Log.d(TAG, "Requesting token from service");
        Intent subscriptionIntent = new Intent();
        subscriptionIntent.setAction(Constants.ACTION_PUSH_REGISTER);
        // TODO look for the matching package and if it is not a system app we ask the user for confirmation once
        subscriptionIntent.setPackage("com.starpush.android.pushservice");
        subscriptionIntent.putExtra(Constants.EXTRA_PACKAGE_NAME, context.getPackageName());
        context.sendBroadcast(subscriptionIntent);
        Log.d(TAG, "Done sending request");

        return null;
    }

    public static void unregister(Context context) {
        Log.d(TAG, "Done sending unsubscribe request");
        Intent subscriptionIntent = new Intent();
        subscriptionIntent.setAction(Constants.ACTION_PUSH_UNREGISTER);
        subscriptionIntent.putExtra(Constants.EXTRA_PACKAGE_NAME, context.getPackageName());
        // TODO look for the matching package and if it is not a system app we ask the user for confirmation once
        subscriptionIntent.setPackage("com.starpush.android.pushservice");
        context.sendBroadcast(subscriptionIntent);
        PushSettingHelper.setPushToken(context, null);
    }
}
